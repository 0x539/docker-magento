FROM debian:buster

ENV DEBIAN_FRONTEND noninteractive

RUN useradd magento
RUN gpasswd -a magento www-data
ADD Magento-CE-2.3.3_sample_data-2019-09-26-05-01-26.tar.bz2 /var/www/magento

WORKDIR /var/www/magento
RUN bash -c 'find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} +'
RUN bash -c 'find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} +'
RUN chown -R magento:www-data .
RUN chmod u+x bin/magento

RUN apt-get update
RUN apt-get install -y nginx php-fpm php7.3-gd php7.3-bcmath php7.3-intl php7.3-json php7.3-mbstring php7.3-mysql php7.3-zip php7.3-opcache php7.3-soap php7.3-xml php7.3-curl

# Nginx
RUN rm /etc/nginx/sites-enabled/default
ADD sites-available/magento /etc/nginx/sites-available/magento
RUN ln -s /etc/nginx/sites-available/magento /etc/nginx/sites-enabled
# RUN service nginx start

# php
RUN sh -c "sed -i -e 's/memory_limit = 128M/memory_limit = 2G/g' /etc/php/7.3/fpm/php.ini"
RUN sh -c "sed -i -e 's/max_execution_time = 30/max_execution_time = 1800/g' /etc/php/7.3/fpm/php.ini"
RUN sh -c "sed -i -e 's/zlib.output_compression = Off/zlib.output_compression = On/g' /etc/php/7.3/fpm/php.ini"
RUN sed -i -e 's/;session.save_path = "\/var\/lib\/php\/sessions"/session.save_path = "\/var\/lib\/php\/session"/g' /etc/php/7.3/fpm/php.ini
RUN sh -c "sed -i -E 's/;(env\[.*\])/\1/g' /etc/php/7.3/fpm/pool.d/www.conf"
RUN mkdir -p /var/lib/php/session
RUN chown -R www-data:www-data /var/lib/php
# RUN service php7.0-fpm start

ADD docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod u+x /docker-entrypoint.sh

EXPOSE 80

ENTRYPOINT ["/docker-entrypoint.sh"]
